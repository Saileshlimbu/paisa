package com.powersoft.paisa.activity;

import android.app.ProgressDialog;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appnext.appnextsdk.API.AppnextAPI;
import com.appnext.appnextsdk.API.AppnextAd;
import com.appnext.appnextsdk.API.AppnextAdRequest;
import com.appnext.core.AppnextError;
import com.baoyz.widget.PullRefreshLayout;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.powersoft.paisa.R;
import com.powersoft.paisa.entity.NativeAds;
import com.powersoft.paisa.service.MessageEvent;
import com.powersoft.paisa.utility.Crypto;
import com.powersoft.paisa.utility.SharedPref;
import com.powersoft.paisa.utility.Utility;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private ArrayList<NativeAds> listAds;
    private TextView txtPoint;
    private ImageView imgError, imgDrawer;
    private AppnextAPI api;
    private DrawerLayout drawerLayout;
    private ProgressDialog dialog;
    private PullRefreshLayout pullRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fresco.initialize(this);

        listView = (ListView) findViewById(R.id.listView);
        txtPoint = (TextView) findViewById(R.id.txtPoint);
        imgError = (ImageView) findViewById(R.id.imgError);
        imgDrawer = (ImageView) findViewById(R.id.imgDrawer);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        pullRefreshLayout = (PullRefreshLayout) findViewById(R.id.pullRefreshLayout);

        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                api.loadAds(new AppnextAdRequest().setCount(10000));
            }
        });

        imgDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });

        new TapTargetSequence(this)
                .targets(
                        TapTarget.forView(findViewById(R.id.imgDrawer), "This is navigation drawer", "Slide your finger left to right to open navigation menu.").cancelable(false),
                        TapTarget.forView(findViewById(R.id.txtPoint), "This is your reward point", "You can easily earn reward point by installing apps").cancelable(false)
                ).start();

        getPoint();

        listAds = new ArrayList<>();

        api = new AppnextAPI(this, getResources().getString(R.string.placement_id));

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading Ads. Please Wait");
        dialog.setCancelable(false);
        dialog.show();
        api.setAdListener(new AppnextAPI.AppnextAdListener() {
            @Override
            public void onAdsLoaded(final ArrayList<AppnextAd> arrayList) {
                dialog.dismiss();
                imgError.setVisibility(View.GONE);
                listAds.clear();
                pullRefreshLayout.setRefreshing(false);
                for (AppnextAd ads : arrayList) {
                    if (!isPackageInstalled(ads.getAdPackage())) {
                        listAds.add(new NativeAds(ads.getAdTitle(), ads.getImageURLWide(), ads.getAdDescription(), ads.getStoreDownloads(), ads.getAdPackage(), ads.getStoreRating(), ads.getAppSize()));
                    }
                }
                ArrayAdapter adapter = new ArrayAdapter<NativeAds>(getApplicationContext(), R.layout.item_ads, listAds) {
                    @NonNull
                    @Override
                    public View getView(final int position, @Nullable View v, @NonNull ViewGroup parent) {
                        v = null;
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                        v = inflater.inflate(R.layout.item_ads, null);
                        SimpleDraweeView image = (SimpleDraweeView) v.findViewById(R.id.imageIcon);
                        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
                        TextView txtDescription = (TextView) v.findViewById(R.id.txtDescription);
                        TextView txtTotalDownloads = (TextView) v.findViewById(R.id.txtDownloads);
                        Button install = (Button) v.findViewById(R.id.btnInstall);
                        final int point = Utility.getRandomNumber();
                        install.setText(point + " Points");

                        image.setImageURI(Uri.parse(listAds.get(position).imageURL));
                        txtTitle.setText(listAds.get(position).adTitle);
                        txtDescription.setText(listAds.get(position).adDescription);
                        txtTotalDownloads.setText(listAds.get(position).storeDownloads + " Downloads");

                        install.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                api.adClicked(arrayList.get(position));
                                api.adImpression(arrayList.get(position));
                                SharedPref.setString(getContext(), Crypto.encrypt("Package"), Crypto.encrypt(listAds.get(position).adPackage));
                                SharedPref.setString(getContext(), Crypto.encrypt("rewardPoint"), Crypto.encrypt(String.valueOf(point)));
                            }
                        });
                        return v;
                    }
                };
                listView.setAdapter(adapter);
            }

            @Override
            public void onError(String s) {
                switch (s) {
                    case AppnextError.NO_ADS:
                        imgError.setVisibility(View.VISIBLE);
                        break;
                    case AppnextError.CONNECTION_ERROR:
                        Log.v("appnext", "connection problem");
                        break;
                    default:
                        Log.v("appnext", "other error");
                }
                dialog.dismiss();
            }
        });

        api.setOnAdOpenedListener(new AppnextAPI.OnAdOpened() {
            @Override
            public void storeOpened() {
                Toast.makeText(MainActivity.this, "Opening Play Store. Please Wait...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String s) {
                Toast.makeText(MainActivity.this, s.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        api.loadAds(new AppnextAdRequest().setCount(10000));
    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        getPoint();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPoint();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        txtPoint.setText(event.point);
    }

    private boolean isPackageInstalled(String packageName) {
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
            if (packageName.equals(packageInfo.packageName)) {
                return true;
            }
        }
        return false;
    }

    private void getPoint() {
        if (SharedPref.isSharedPrefSet(this)) {
            txtPoint.setText(Crypto.decrypt(SharedPref.getString(this, Crypto.encrypt("Point"))));
        }
    }
}

