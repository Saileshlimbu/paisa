package com.powersoft.paisa.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.widget.Toast;

import com.powersoft.paisa.utility.Crypto;
import com.powersoft.paisa.utility.SharedPref;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by sailesh on 6/17/17.
 */

public class AppListener extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo appInfo = null;
            try {
                String encryptedPackageName = SharedPref.getString(context, Crypto.encrypt("Package"));
                appInfo = pm.getApplicationInfo(Crypto.decrypt(encryptedPackageName), 0);
                if (appInfo.packageName.equals(Crypto.decrypt(encryptedPackageName))) {
                    String encryptedRewardPoint = SharedPref.getString(context, Crypto.encrypt("rewardPoint"));
                    String rewardPoint = Crypto.decrypt(encryptedRewardPoint);
                    if (SharedPref.isSharedPrefSet(context)) {
                        String encryptedPreviousPoint = SharedPref.getString(context, Crypto.encrypt("Point"));
                        String previousPoint = Crypto.decrypt(encryptedPreviousPoint);
                        int finalPoint = Integer.parseInt(previousPoint) + Integer.parseInt(rewardPoint);
                        SharedPref.setString(context, Crypto.encrypt("Point"), Crypto.encrypt(String.valueOf(finalPoint)));
                        EventBus.getDefault().post(new MessageEvent(String.valueOf(finalPoint)));
                    } else {
                        SharedPref.setString(context, Crypto.encrypt("Point"), Crypto.encrypt(String.valueOf(rewardPoint)));
                        EventBus.getDefault().post(new MessageEvent(String.valueOf(rewardPoint)));
                    }
                    Toast.makeText(context, "Congratulation you have successfully earned " + rewardPoint + " Points.", Toast.LENGTH_SHORT).show();
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
