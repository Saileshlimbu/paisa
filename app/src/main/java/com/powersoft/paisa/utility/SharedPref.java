package com.powersoft.paisa.utility;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPref {
    private static SharedPreferences sharedPreferences;

    public static void init(Context context) {
        sharedPreferences = context.getSharedPreferences("My_Prefs", MODE_PRIVATE);
    }

    public static boolean isSharedPrefSet(Context context) {
        init(context);
        if (sharedPreferences.contains(Crypto.encrypt("Point"))) {
            return true;
        }
        return false;
    }

    public static void setString(Context context, String key, String value) {
        init(context);
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.putString(key, value);
        ed.commit();
    }

    public static String getString(Context context, String key) {
        init(context);
        return sharedPreferences.getString(key, "");
    }
}
