package com.powersoft.paisa.entity;

/**
 * Created by sailesh on 6/13/17.
 */

public class NativeAds {
    public String adTitle;
    public String imageURL;
    public String adDescription;
    public String storeDownloads;
    public String adPackage;
    public String storeRating;
    public String appSize;

    public NativeAds(String adTitle, String imageURL, String adDescription, String storeDownloads, String adPackage, String storeRating, String appSize) {
        this.adTitle = adTitle;
        this.imageURL = imageURL;
        this.adDescription = adDescription;
        this.storeDownloads = storeDownloads;
        this.adPackage = adPackage;
        this.storeRating = storeRating;
        this.appSize = appSize;
    }
}
